FROM arm32v7/debian

MAINTAINER Sidney Silva <prosidney@gmail.com>

RUN  apt-get update && apt-get install -y wget -y gnupg

RUN echo "deb http://security.debian.org/debian-security wheezy/updates main " > /etc/apt/sources.list
RUN echo "deb http://ftp.de.debian.org/debian sid main " > /etc/apt/sources.list
RUN echo "deb http://ftp.de.debian.org/debian wheezy main  " > /etc/apt/sources.list
RUN echo "deb http://ftp.de.debian.org/debian jessie main   " > /etc/apt/sources.list
RUN apt-get update -y

RUN apt-get install daemon
RUN apt-get install psmisc
RUN apt-get install net-tools
RUN apt-get install libtinfo5=5.9+20140913-1+deb8u3 -y --allow-remove-essential --allow-downgrades
RUN apt-get install libncurses5 -y
RUN apt-get install sysvinit-utils -y --allow-remove-essential
RUN apt-get install sysv-rc -y
RUN apt-get install initscripts -y
RUN apt-get install procps

RUN wget -q -O - https://jenkins-ci.org/debian/jenkins-ci.org.key | apt-key add -
RUN echo "deb http://pkg.jenkins-ci.org/debian binary/ " > /etc/apt/sources.list
RUN apt-get update -y && apt-get -y upgrade
RUN apt-get install jenkins -y

RUN systemctl status jenkins.service

EXPOSE 8080

ENTRYPOINT ["run.bash"]
